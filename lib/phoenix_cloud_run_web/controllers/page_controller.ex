defmodule PhoenixCloudRunWeb.PageController do
  use PhoenixCloudRunWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
