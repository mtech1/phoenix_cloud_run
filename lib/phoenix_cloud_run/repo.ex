defmodule PhoenixCloudRun.Repo do
  use Ecto.Repo,
    otp_app: :phoenix_cloud_run,
    adapter: Ecto.Adapters.Postgres
end
