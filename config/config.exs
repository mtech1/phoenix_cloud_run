# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :phoenix_cloud_run,
  ecto_repos: [PhoenixCloudRun.Repo]

# Configures the endpoint
config :phoenix_cloud_run, PhoenixCloudRunWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WTc4DHgstTVCt7C3nhHJgzpw5wZO6qQlgAIYhZD8fclD8n11q2bwceQRTd5SL4aF",
  render_errors: [view: PhoenixCloudRunWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: PhoenixCloudRun.PubSub,
  live_view: [signing_salt: "FcwbKzT9"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
